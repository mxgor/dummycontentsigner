package main

import (
	"fmt"
	"sync"

	"github.com/kelseyhightower/envconfig"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-issuance/modules/dummycontentsigner/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-issuance/modules/dummycontentsigner/issuance"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-issuance/modules/dummycontentsigner/metadata"
)

var conf config.Config

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	if err := envconfig.Process("", &conf); err != nil {
		panic(fmt.Sprintf("failed to load config from env: %+v", err))
	}

	storage := new(issuance.DummyStorage)

	//publish metadata
	go metadata.Publish(conf)

	//reply to credential request
	go issuance.CredentialReply(conf, storage)

	go issuance.CredentialRequest(conf, storage)

	wg.Wait()
}
